import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Request, Response } from 'express';

import { DBError } from 'db-errors';
import { QueryFailedError } from 'typeorm';
import { IQueryFailed } from './dtos/IDBError';

@Catch(HttpException, InternalServerErrorException)
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();
    const request = context.getRequest<Request>();
    const status = exception.getStatus();

    console.error(exception);

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      message: exception.message,
      stackForDev: exception.stack,
    });
  }
}

@Catch(DBError, QueryFailedError)
export class AllExceptionsDBFilter implements ExceptionFilter {
  catch(exception: IQueryFailed, host: ArgumentsHost) {
    let mensagemDB = '';
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();
    const request = context.getRequest<Request>();

    switch (exception.code) {
      case '23000':
        mensagemDB = `Essa operação viola Integrity Constraint Violation na tabela ${exception.table}`;
        break;
      case '23001':
        mensagemDB = `Essa operação viola Restrict Violation na tabela ${exception.table}`;
        break;
      case '23502':
        mensagemDB = `Essa operação viola Not Null Violation na tabela ${exception.table}`;
        break;
      case '23503':
        mensagemDB =
          'Não é possivel excluir um registro que já está sendo utilizado';
        break;
      case '23505':
        mensagemDB = `Essa operação viola UniqueViolation na tabela ${exception.table}`;
        break;
      case '23514':
        mensagemDB = `Essa operação viola Check Violation na tabela ${exception.table}`;
        break;
      case '23P01':
        mensagemDB = `Essa operação viola Exclusion Violation  na tabela ${exception.table}`;
        break;
      default:
        mensagemDB =
          'Erro desconhecido no banco de dados. Contate o administrador';
    }

    response.status(400).json({
      statusCode: 400,
      timestamp: new Date().toISOString(),
      path: request.url,
      dbCodeError: exception.code,
      dbNameError: exception.name,
      message: mensagemDB,
      stackForDev: exception.stack,
    });
  }
}
