import { Module } from '@nestjs/common';
import { JobsModule } from '../modules/jobs';
import { TasksService } from './tasks.service';

@Module({
  providers: [TasksService],
  imports: [JobsModule],
})
export class TasksModule {}
