import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { JobsService } from '../modules/jobs/services';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private jobsService: JobsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async startTaskByHour() {
    this.logger.debug('Service getJobsAndRun called --> ');
    await this.jobsService.getJobsAndRun();
  }
}
