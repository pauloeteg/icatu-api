import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { NextFunction } from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  const writeLog = (req: Request, res: Response, next: NextFunction) => {
    Logger.log(`Rota acessada: ${req.url}`);
    Logger.log(`Metodo: ${req.method}`);
    Logger.log(`header: ${JSON.stringify(req.headers)}`);
    next();
  };

  app.use(writeLog);

  await app.listen(5000, () =>
    Logger.log(`[NestApplication] Servidor up na porta: 5000`),
  );
}
bootstrap();
