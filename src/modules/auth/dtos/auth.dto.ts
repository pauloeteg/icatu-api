export interface IAuth {
  email: string;
  password: string;
}

export interface ITokenUserAuthenticated {
  access_token: string;
}
