import { Controller, Post, HttpStatus, HttpCode, Body } from '@nestjs/common';
import { IAuth, ITokenUserAuthenticated } from '../dtos/auth.dto';
import { AuthService } from '../services';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('auth/login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() dto: IAuth): Promise<ITokenUserAuthenticated> {
    return this.authService.login(dto);
  }
}
