import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../../user/services';
import { JwtService } from '@nestjs/jwt';
import { UtilsService } from 'src/utils';
import { IAuth, ITokenUserAuthenticated } from '../dtos/auth.dto';
import { User } from 'src/modules/user/entities';

@Injectable()
export class AuthService {
  private readonly msgErrorAuth = 'Credenciais inválidas';
  private readonly msgErrorFieldMandatory = 'Email e senha obrigatório.';
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(user: IAuth): Promise<ITokenUserAuthenticated> {
    if (!user.password || !user.email) {
      throw new HttpException(
        this.msgErrorFieldMandatory,
        HttpStatus.UNAUTHORIZED,
      );
    }

    const userIsOk = await this.validateUser(user);
    const payload = { email: userIsOk.email, name: userIsOk.name };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(user: IAuth): Promise<User> {
    const foundUser = await this.usersService.findByEmail(user.email);

    if (!foundUser || !foundUser.email || !foundUser.password) {
      throw new HttpException(this.msgErrorAuth, HttpStatus.UNAUTHORIZED);
    }

    const isPasswordValid = await UtilsService.validateHash(
      user.password,
      foundUser.password,
    );

    if (!isPasswordValid) {
      throw new HttpException(this.msgErrorAuth, HttpStatus.UNAUTHORIZED);
    }

    return foundUser;
  }
}
