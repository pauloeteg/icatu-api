import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AllExceptionsDBFilter } from 'src/exceptions/all-exceptions-filter';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { Job } from '../entities';
import { JobsService } from '../services';

@UseGuards(JwtAuthGuard)
@UseFilters(AllExceptionsDBFilter)
@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @Get()
  async findAll(): Promise<Job[]> {
    return this.jobsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<Job> {
    return this.jobsService.findOne(id);
  }

  @Post()
  async create(@Body() dto: Job) {
    return this.jobsService.create(dto);
  }

  @Delete(':id')
  async delete(@Param('id', ParseUUIDPipe) id: string) {
    return this.jobsService.delete(id);
  }

  @Put(':id')
  async updateJob(@Param('id', ParseUUIDPipe) id: string, @Body() dto: Job) {
    return this.jobsService.updateJob(id, dto);
  }
}
