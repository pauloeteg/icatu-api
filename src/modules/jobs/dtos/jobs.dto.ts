import { Job } from '../entities';

export class JobDto {
  name?: string;
  time?: number;
  startDate?: Date;
  customer?: string;

  constructor(job: Job | undefined) {
    if (job) {
      this.name = job?.name;
      this.time = job?.time;
      this.startDate = job?.startDate;
      this.customer = job?.customer;
    }
  }
}
