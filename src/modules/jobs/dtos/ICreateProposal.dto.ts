import { ITicket } from 'src/api/dto/response-tickets.dto';
import { Job } from '../entities';

export interface ICreateProposalDto {
  ticket: ITicket;
  jobDb: Job;
  index: number;
}
