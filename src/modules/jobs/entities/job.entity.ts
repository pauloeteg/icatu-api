import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { JobDto } from '../dtos';
import { Layout } from 'src/modules/layouts/entities';

@Entity()
export class Job {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
  })
  createdAt?: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  updatedAt?: Date;

  @Column({ length: 300, nullable: false })
  name?: string;

  @Column({
    nullable: false,
  })
  time?: number;

  @Column({
    type: 'date',
    nullable: true,
  })
  startDate?: Date;

  @Column({ nullable: false })
  customer?: string;

  @ManyToOne(() => Layout, (layout: Layout) => layout.job, {
    eager: true,
    nullable: true,
  })
  @JoinColumn()
  layout?: Layout;

  dtoClass = JobDto;
}
