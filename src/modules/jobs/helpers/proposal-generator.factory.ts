import { HttpStatus, Injectable } from '@nestjs/common';
import { ICreateProposalDto } from '../dtos/ICreateProposal.dto';
import { TipoLayoutEnum } from '../dtos/TipoLayoutEnum';
import { ProposalCapGenerate } from './generate-proposal-cap.factory';
import { ProposalVidaGenerate } from './generate-proposal-vida.factory';

@Injectable()
export class ProposalGeneratorFactory {
  constructor(
    private readonly proposalCapGenerate: ProposalCapGenerate,
    private readonly proposalVidaGenerate: ProposalVidaGenerate,
  ) {}

  public async build(dto: ICreateProposalDto) {
    console.log('jobDb.layout.type é: ', dto.jobDb.layout.type);

    switch (dto.jobDb.layout.type) {
      case TipoLayoutEnum.CAP:
        this.proposalCapGenerate.create(dto);
        break;

      case TipoLayoutEnum.VIDA:
        this.proposalVidaGenerate.create(dto);
        break;

      default:
        console.error(
          `Não existe um gerador para o tipo de layout informado: ${dto.jobDb.layout.type}`,
          HttpStatus.BAD_REQUEST,
        );
    }
  }
}
