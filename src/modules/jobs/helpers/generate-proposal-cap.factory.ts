import { Injectable } from '@nestjs/common';
import { UtilsService } from 'src/utils';
import { ICreateProposalDto } from '../dtos/ICreateProposal.dto';
import { TypeFieldEnum } from 'src/modules/layouts/models/ILayouts';
import { format } from 'date-fns';
@Injectable()
export class ProposalCapGenerate {
  async create(dto: ICreateProposalDto) {
    ///// geraNomedoArquivo
    const nameOfFile = this.genNameOfFile(dto);

    ///// aplicaRegraEGeraTexto
    const contentDock = this.getTextDoc(dto);

    //// Cria arquivo
    UtilsService.createFileOfProposal(nameOfFile, contentDock);

    //// envia sftp
    // IMPLEMENTAR
  }

  private getTextDoc(dto: ICreateProposalDto): string | undefined {
    if (!dto.jobDb || !dto.ticket) {
      return undefined;
    }

    let stringMontada = '';
    dto.jobDb.layout.layoutJson.map(async (campo, index) => {
      /// DESMONTAR ESSE BLOCO DESNECESSÁRIO

      if (campo.typeField === TypeFieldEnum.HEADER) {
        stringMontada += '\n\r';
      }

      // Se aberto aplica as regras de aberto
      if (campo.typeField === TypeFieldEnum.ABERTO) {
        /// SE FIXO, SETA VALOR FIXO
        if (campo.fixed === 'SIM' && campo.default != '') {
          stringMontada += campo.default += ';';
        }

        /// SE NÃO, PEGA O VALOR DO CUSTOM FIELD SETADO
        if (campo.fixed === 'NAO' && campo.customFieldNumber != undefined) {
          stringMontada += UtilsService.getValueCustomField(
            campo.customFieldNumber,
            dto.ticket,
          );
        }
      }

      /// SE FILLER, PREENCHE COM O TAMANHO DO FILLER
      if (
        campo.typeField === TypeFieldEnum.FILLER &&
        Number(campo.fillerSize) > 0
      ) {
        stringMontada += ' '.repeat(Number(campo.fillerSize));
      }

      //// Se nulo apenas ;
      if (campo.typeField === TypeFieldEnum.NULO) {
        stringMontada += ';';
      }

      //// Se sequencial faz o fill com o tamanho do sequencial
      if (campo.typeField === TypeFieldEnum.SEQUENCIAL) {
        stringMontada += UtilsService.zeroFill(index, dto.index);
        stringMontada += ';';
      }

      //// Se data dinamica, monto no formato do formatDate
      if (campo.typeField === TypeFieldEnum.DATA_DINAMICA) {
        stringMontada += format(new Date(), campo.formatDate);
        stringMontada += ';';
      }
    });

    return stringMontada;
  }

  private genNameOfFile(dto: ICreateProposalDto) {
    let nameOfFile = 'capSemNome';

    // Ano da geração do arquivo
    nameOfFile += UtilsService.zeroFill(new Date().getFullYear(), 2);

    // Mês da geração do arquivo
    nameOfFile += UtilsService.zeroFill(new Date().getMonth() + 1, 2);

    // Dia da geração do arquivo
    nameOfFile += UtilsService.zeroFill(new Date().getDate(), 2);

    // Seqüencial do arquivo para o dia
    nameOfFile += '.' + UtilsService.zeroFill(dto.index, 3);
    return nameOfFile;
  }
}
