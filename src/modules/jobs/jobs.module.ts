import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZendeskApi } from 'src/api';
import { JobsController } from './controllers';
import { ProposalCapGenerate } from './helpers/generate-proposal-cap.factory';
import { ProposalVidaGenerate } from './helpers/generate-proposal-vida.factory';
import { ProposalGeneratorFactory } from './helpers/proposal-generator.factory';
import { JobsRepository } from './repositories';
import { JobsService } from './services';

const jobsRepoDynamicModule = TypeOrmModule.forFeature([JobsRepository]);

@Module({
  imports: [jobsRepoDynamicModule],
  exports: [JobsService, jobsRepoDynamicModule],
  providers: [
    JobsService,
    ZendeskApi,
    ProposalGeneratorFactory,
    ProposalCapGenerate,
    ProposalVidaGenerate,
  ],
  controllers: [JobsController],
})
export class JobsModule {}
