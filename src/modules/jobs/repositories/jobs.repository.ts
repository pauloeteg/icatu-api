import { EntityRepository, Repository } from 'typeorm';
import { Job } from '../entities';

@EntityRepository(Job)
export class JobsRepository extends Repository<Job> {}
