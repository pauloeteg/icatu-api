import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ZendeskApi } from 'src/api';
import { Not } from 'typeorm';
import { Job } from '../entities';
import { ProposalGeneratorFactory } from '../helpers/proposal-generator.factory';
import { JobsRepository } from '../repositories';

@Injectable()
export class JobsService {
  constructor(
    private readonly jobsRepository: JobsRepository,
    private readonly zendeskApi: ZendeskApi,
    private readonly proposalFactory: ProposalGeneratorFactory,
  ) {}

  async findAll(): Promise<Job[] | undefined> {
    return await this.jobsRepository.find();
  }

  async findOne(id: string): Promise<Job | undefined> {
    return await this.jobsRepository.findOne(id);
  }

  async create(job: Job): Promise<Job> {
    if (!job.name || !job.customer || !job.time) {
      throw new HttpException(
        'Por favor, insira todas as informações.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const jobAlreadyExist = await this.jobsRepository.findOne({
      where: { name: job.name },
    });

    if (jobAlreadyExist) {
      throw new HttpException(
        'Por favor, selecione outro nome e tente novamente.',
        HttpStatus.BAD_REQUEST,
      );
    }

    job.startDate = new Date();

    return await this.jobsRepository.save(job);
  }

  async findOneOrFail(id: string) {
    const user = await this.jobsRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('Job não encontrado.');
    }

    return user;
  }

  async delete(id: string): Promise<Job | undefined> {
    const jobAlreadyExist = await this.findOneOrFail(id);

    await this.jobsRepository.delete({ id });

    return jobAlreadyExist;
  }

  async updateJob(id: string, newJob: Job): Promise<Job> {
    const currentJob = await this.findOneOrFail(id);

    if (!currentJob) {
      throw new HttpException('Job não encontrado.', HttpStatus.BAD_REQUEST);
    }

    const jobAlreadyExist = await this.jobsRepository.findOne({
      where: { name: newJob.name, id: Not(newJob.id) },
    });

    if (jobAlreadyExist) {
      throw new HttpException(
        'Por favor, selecione outro nome e tente novamente.',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (newJob.name) {
      currentJob.name = newJob.name;
    }
    if (newJob.customer) {
      currentJob.customer = newJob.customer;
    }
    if (newJob.startDate) {
      currentJob.startDate = newJob.startDate;
    }

    if (newJob.time) {
      currentJob.time = newJob.time;
    }

    if (newJob.layout) {
      currentJob.layout = newJob.layout;
    }

    return await this.jobsRepository.save(currentJob);
  }

  async getJobsAndRun() {
    const currentTime = new Date().getHours();

    const jobsToRunNow = await this.jobsRepository.find({
      where: { time: currentTime },
    });

    console.log(
      `\nJobs cadastrados para rodar agora, às ${currentTime} :: ${jobsToRunNow.length}`,
    );

    jobsToRunNow.forEach(async (jobDb) => {
      try {
        const tickets = await this.zendeskApi.getAllTicketsByFilter(
          jobDb.customer,
          jobDb.startDate,
        );

        tickets.map(async (ticket, index) => {
          this.proposalFactory.build({
            index,
            jobDb,
            ticket,
          });
        });
      } catch (err) {
        console.error('Ocorreu um erro na chamada ao zendesk --> \n', err);
      }
    });
  }
}
