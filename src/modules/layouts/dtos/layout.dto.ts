import { Layout } from '../entities';
import { IJson } from '../models/ILayouts';

export class LayoutDto {
  id?: string;
  name?: string;
  layoutJson?: IJson[];
  type?: string;

  constructor(layout: Layout | undefined) {
    if (layout) {
      this.layoutJson = layout?.layoutJson;
      this.name = layout?.name;
      this.type = layout?.type;
    }
  }
}
