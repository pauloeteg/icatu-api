import { Job } from 'src/modules/jobs/entities';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { LayoutDto } from '../dtos';
import { IJson } from '../models/ILayouts';

@Entity()
export class Layout {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
  })
  createdAt?: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  updatedAt?: Date;

  @Column({ length: 50, nullable: false })
  name?: string;

  @Column({ length: 50, nullable: false })
  type?: string;

  @Column({ nullable: false, type: 'jsonb', default: [] })
  layoutJson?: IJson[];

  @OneToMany(() => Job, (job: Job) => job.layout, {
    nullable: true,
    lazy: true,
  })
  @JoinColumn()
  job?: Job;

  dtoClass = LayoutDto;
}
