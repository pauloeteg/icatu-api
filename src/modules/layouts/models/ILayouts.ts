import { TipoLayoutEnum } from 'src/modules/jobs/dtos/TipoLayoutEnum';

export interface ILayout {
  id?: string;
  name?: string;
  type?: TipoLayoutEnum | undefined;
  layoutJson?: IJson[];
  createdAt?: string;
}

export interface IJson {
  name: string | undefined;
  type: string | undefined;
  size: string | undefined;
  mandatory: 'SIM' | 'NAO';
  fixed: 'SIM' | 'NAO';
  typeField: TypeFieldEnum | undefined;
  default: string | undefined;
  customFieldNumber: string | undefined;
  fillerSize?: string | undefined;
  sequencialSize?: string | undefined;
  formatDate?: string | undefined;
}

export enum TypeFieldEnum {
  ABERTO = 'ABERTO',
  NULO = 'NULO',
  FILLER = 'FILLER',
  SEQUENCIAL = 'SEQUENCIAL',
  DATA_DINAMICA = 'DATA_DINAMICA',
  HEADER = 'HEADER',
}
