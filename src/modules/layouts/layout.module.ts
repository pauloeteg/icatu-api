import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZendeskApi } from 'src/api';
import { LayoutsControllers } from './controllers';
import { LayoutRepository } from './repositories';
import { LayoutsService } from './services';

const layoutRepoDynamicModule = TypeOrmModule.forFeature([LayoutRepository]);

@Module({
  imports: [layoutRepoDynamicModule],
  exports: [LayoutsService, layoutRepoDynamicModule],
  providers: [LayoutsService, ZendeskApi],
  controllers: [LayoutsControllers],
})
export class LayoutsModule {}
