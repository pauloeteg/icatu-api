import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Put,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AllExceptionsDBFilter } from 'src/exceptions/all-exceptions-filter';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { Layout } from '../entities';
import { IJson } from '../models/ILayouts';
import { LayoutsService } from '../services';

@UseGuards(JwtAuthGuard)
@Controller('layouts')
@UseFilters(AllExceptionsDBFilter)
export class LayoutsControllers {
  constructor(private readonly layoutsService: LayoutsService) {}

  @Get()
  async findAll(): Promise<Layout[]> {
    return this.layoutsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<Layout> {
    return this.layoutsService.findOne(id);
  }

  @Post()
  async create(@Body() dto: Layout) {
    return this.layoutsService.create(dto);
  }

  @Delete(':id')
  async delete(@Param('id', ParseUUIDPipe) id: string) {
    return this.layoutsService.delete(id);
  }

  @Post('duplicate-layout')
  async duplicateLayout(@Body() id: string) {
    return this.layoutsService.duplicateLayout(id);
  }

  @Put('insert-field/:id')
  async insertField(
    @Body() dto: IJson,
    @Param('id', ParseUUIDPipe) id: string,
  ) {
    return this.layoutsService.insertField(id, dto);
  }

  @Put('delete-field/:id')
  async deleteField(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() dto: { posField: number },
  ) {
    return this.layoutsService.deleteField(id, dto);
  }

  @Patch('update-name/:id')
  async updateName(@Body() dto: { id: string; name: string }) {
    return this.layoutsService.updateName(dto);
  }

  @Patch('update-field')
  async updateField(
    @Body() dto: { idLayout: string; posField: number; field: IJson },
  ) {
    return this.layoutsService.updateField(dto);
  }

  @Patch('move-field')
  async moveField(@Body() dto: { idLayout: string; from: number; to: number }) {
    return this.layoutsService.moveField(dto);
  }
}
