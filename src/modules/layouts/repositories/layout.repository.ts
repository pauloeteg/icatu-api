import { EntityRepository, Repository } from 'typeorm';
import { Layout } from '../entities';

@EntityRepository(Layout)
export class LayoutRepository extends Repository<Layout> {}
