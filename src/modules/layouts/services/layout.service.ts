import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UtilsService } from 'src/utils';
import { LayoutDto } from '../dtos';
import { Layout } from '../entities';
import { IJson, TypeFieldEnum } from '../models/ILayouts';
import { LayoutRepository } from '../repositories';

@Injectable()
export class LayoutsService {
  constructor(private readonly layoutRepository: LayoutRepository) {}

  async findAll(): Promise<Layout[] | undefined> {
    return await this.layoutRepository.find();
  }

  async findOne(id: string): Promise<Layout | undefined> {
    return await this.layoutRepository.findOne(id);
  }

  async create(layout: Layout): Promise<Layout> {
    return await this.layoutRepository.save(layout);
  }

  async findOneOrFail(id: string) {
    const layout = await this.layoutRepository.findOne(id);

    if (!layout) {
      throw new NotFoundException('Layout não encontrado.');
    }

    return layout;
  }

  async delete(id: string): Promise<Layout | undefined> {
    const layoutAlreadyExist = await this.findOneOrFail(id);

    await this.layoutRepository.delete({ id });

    return layoutAlreadyExist;
  }

  async duplicateLayout(id: string) {
    const layout = await this.findOneOrFail(id);
    const newLayout = new LayoutDto(layout);
    newLayout.name = newLayout.name + ' copy';
    return await this.layoutRepository.save(newLayout);
  }

  async updateName(dto: { id: string; name: string }) {
    const layoutInDb = await this.findOne(dto.id);
    if (dto.name && dto.name !== layoutInDb.name) {
      layoutInDb.name = dto.name;
      const newLayout = new LayoutDto(layoutInDb);
      return await this.layoutRepository.update(dto.id, newLayout);
    }
  }

  async insertField(id: string, dto: IJson) {
    const layoutInDb = await this.findOne(id);
    if (!dto.name || !dto.typeField) {
      throw new HttpException(
        'O nome do campo é obrigatório.',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (dto.fixed === 'NAO') {
      dto.default = undefined;
    }

    if (dto.fixed === 'SIM') {
      dto.customFieldNumber = undefined;
    }

    if (dto.fixed === 'SIM' && !dto.default) {
      throw new HttpException(
        'Se o campo é fixo, por favor, insira o valor padrão',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (dto.typeField === TypeFieldEnum.NULO) {
      dto = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.NULO,
        name: dto.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    if (dto.typeField === TypeFieldEnum.SEQUENCIAL) {
      dto = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.SEQUENCIAL,
        name: dto.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: dto.sequencialSize,
      };
    }

    if (dto.typeField === TypeFieldEnum.DATA_DINAMICA) {
      dto = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.DATA_DINAMICA,
        name: dto.name,
        fillerSize: undefined,
        formatDate: dto.formatDate,
        sequencialSize: undefined,
      };
    }

    if (dto.typeField === TypeFieldEnum.FILLER) {
      dto = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.FILLER,
        name: dto.name,
        fillerSize: dto.fillerSize,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    if (dto.typeField === TypeFieldEnum.ABERTO) {
      dto = {
        customFieldNumber: dto.customFieldNumber,
        default: dto.default,
        type: dto.type,
        size: dto.size,
        mandatory: dto.mandatory,
        fixed: dto.fixed,
        typeField: TypeFieldEnum.ABERTO,
        name: dto.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    layoutInDb.layoutJson.push(dto);
    const newLayout = new LayoutDto(layoutInDb);
    return await this.layoutRepository.update(id, newLayout);
  }

  async updateField(dto: { idLayout: string; posField: number; field: IJson }) {
    const layoutInDb = await this.findOne(dto.idLayout);

    if (!dto.field.name || !dto.field.typeField) {
      throw new HttpException(
        'O nome do campo é obrigatório.',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (dto.field.fixed === 'SIM') {
      dto.field.customFieldNumber = undefined;
    }

    if (dto.field.fixed === 'NAO') {
      dto.field.default = undefined;
    }

    /// criar novo objeto, não é bom alterar o que vem.

    ///////// ISSO PODE SER MENOR/MELHOR
    if (dto.field.typeField === TypeFieldEnum.NULO) {
      dto.field = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.NULO,
        name: dto.field.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    if (dto.field.typeField === TypeFieldEnum.SEQUENCIAL) {
      dto.field = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.SEQUENCIAL,
        name: dto.field.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: dto.field.sequencialSize,
      };
    }

    if (dto.field.typeField === TypeFieldEnum.FILLER) {
      dto.field = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.FILLER,
        name: dto.field.name,
        fillerSize: dto.field.fillerSize,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    if (dto.field.typeField === TypeFieldEnum.DATA_DINAMICA) {
      dto.field = {
        customFieldNumber: undefined,
        default: undefined,
        type: undefined,
        size: undefined,
        mandatory: undefined,
        fixed: undefined,
        typeField: TypeFieldEnum.DATA_DINAMICA,
        name: dto.field.name,
        fillerSize: undefined,
        formatDate: dto.field.formatDate,
        sequencialSize: undefined,
      };
    }

    if (dto.field.typeField === TypeFieldEnum.ABERTO) {
      dto.field = {
        customFieldNumber: dto.field.customFieldNumber,
        default: dto.field.default,
        type: dto.field.type,
        size: dto.field.size,
        mandatory: dto.field.mandatory,
        fixed: dto.field.fixed,
        typeField: TypeFieldEnum.ABERTO,
        name: dto.field.name,
        fillerSize: undefined,
        formatDate: undefined,
        sequencialSize: undefined,
      };
    }

    layoutInDb.layoutJson[dto.posField] = dto.field;
    const newLayout = new LayoutDto(layoutInDb);
    return await this.layoutRepository.update({ id: dto.idLayout }, newLayout);
  }

  async deleteField(id: string, dto: { posField: number }) {
    const layoutAlreadyExist = await this.findOneOrFail(id);
    layoutAlreadyExist.layoutJson.splice(dto.posField, 1);
    return await this.layoutRepository.save(layoutAlreadyExist);
  }

  async moveField(dto: { idLayout: string; from: number; to: number }) {
    const layoutInDb = await this.findOne(dto.idLayout);

    layoutInDb.layoutJson = UtilsService.changePositionInArray(
      layoutInDb.layoutJson,
      dto.from,
      dto.to,
    );

    const newLayout = new LayoutDto(layoutInDb);
    return await this.layoutRepository.update({ id: dto.idLayout }, newLayout);
  }
}
