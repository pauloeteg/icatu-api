import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './controllers';
import { UserRepository } from './repositories';
import { UsersService } from './services';

const userRepoDynamicModule = TypeOrmModule.forFeature([UserRepository]);

@Module({
  imports: [userRepoDynamicModule],
  exports: [UsersService, userRepoDynamicModule],
  providers: [UsersService],
  controllers: [UserController],
})
export class UsersModule {}
