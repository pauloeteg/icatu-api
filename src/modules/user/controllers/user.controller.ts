import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AllExceptionsDBFilter } from 'src/exceptions/all-exceptions-filter';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { UserDto } from '../dtos';
import { User } from '../entities';
import { UsersService } from '../services/';

@UseFilters(AllExceptionsDBFilter)
@Controller('users')
export class UserController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string): Promise<User> {
    return this.usersService.findOne(id);
  }

  @Post()
  async create(@Body() dto: UserDto) {
    return this.usersService.create(dto);
  }

  @Delete(':id')
  async delete(@Param('id', ParseUUIDPipe) id: string) {
    return this.usersService.delete(id);
  }

  @Put(':id')
  async update(@Param('id', ParseUUIDPipe) id: string, @Body() dto: UserDto) {
    const userUpdated = this.usersService.update(id, dto);
    return userUpdated;
  }
}
