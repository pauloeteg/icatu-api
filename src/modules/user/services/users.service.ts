import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { UtilsService } from 'src/utils';
import { UserDto } from '../dtos';
import { User } from '../entities';
import { UserRepository } from '../repositories';

@Injectable()
@UseGuards(JwtAuthGuard)
export class UsersService {
  constructor(private readonly userRepository: UserRepository) {}

  async findAll() {
    return await this.userRepository.find();
  }

  async findOne(id: string): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  async findOneOrFail(id: string) {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('Usuário não encontrado.');
    }

    return user;
  }

  async findByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne({ where: { email: email } });
  }

  async create(user: UserDto): Promise<User> {
    if (!user.email || !user.name) {
      throw new HttpException(
        'O email e nome são obrigatórios',
        HttpStatus.BAD_REQUEST,
      );
    }

    const userAlreadyExist = await this.userRepository.findOne({
      where: { email: user.email },
    });

    if (userAlreadyExist) {
      throw new HttpException(
        'Email indisponível, por favor selecione outro endereço de email e tente novamente.',
        HttpStatus.BAD_REQUEST,
      );
    }

    user.password = UtilsService.generateHash(user.password);

    return await this.userRepository.save(user);
  }

  async delete(id: string): Promise<User> {
    const userAlreadyExist = await this.findOneOrFail(id);

    await this.userRepository.delete({ id });

    return userAlreadyExist;
  }

  async update(id: string, dto: UserDto): Promise<User> {
    const currentUser = await this.findOneOrFail(id);

    currentUser.name = dto.name;
    currentUser.email = dto.email;

    return await this.userRepository.save(currentUser);
  }
}
