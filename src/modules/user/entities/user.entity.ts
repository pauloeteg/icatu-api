import { UserDto } from '../dtos';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
  })
  createdAt?: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  updatedAt?: Date;

  @Column({ length: 300, nullable: false })
  name?: string;

  @Column({ length: 50, nullable: false })
  email?: string;

  @Column({ length: 75, nullable: true })
  password?: string;

  dtoClass = UserDto;
}
