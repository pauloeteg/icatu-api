import { User } from '../entities';

export class UserDto {
  name?: string;
  email?: string;
  password?: string;

  constructor(user: User | undefined) {
    if (user) {
      this.email = user?.email;
      this.name = user?.name;
    }
  }
}
