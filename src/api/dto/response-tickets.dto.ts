interface ISource {
  from: any;
  to: any;
  rel: any;
}

interface IVia {
  channel: string;
  source: ISource;
}

export interface IFields {
  id: number;
  value: string;
}

export interface ITicket {
  url: 'string';
  id: number;
  external_id: string;
  via: IVia;
  created_at: Date;
  updated_at: Date;
  type: string;
  subject: string;
  raw_subject: string;
  description: string;
  priority: string;
  status: string;
  recipient: string;
  requester_id: number;
  submitter_id: number;
  assignee_id: number;
  organization_id: string;
  group_id: number;
  collaborator_ids: [];
  follower_ids: [];
  email_cc_ids: [];
  forum_topic_id: string;
  problem_id: string;
  has_incidents: boolean;
  is_public: boolean;
  due_at: string;
  tags: string[];
  custom_fields: IFields[];
  satisfaction_rating: null;
  sharing_agreement_ids: [];
  fields: IFields[];
  followup_ids: [];
  ticket_form_id: number;
  brand_id: number;
  allow_channelback: boolean;
  allow_attachments: boolean;
}
