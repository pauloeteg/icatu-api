import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { encode } from 'base-64';
import { ITicket } from './dto/response-tickets.dto';

export class ZendeskApi {
  private readonly baseUrl: string | undefined;
  private readonly token: string | undefined;
  private readonly email: string | undefined;

  private readonly apiZendesk;

  constructor() {
    const configService = new ConfigService();
    const subdomain = configService.get('SUBDOMAIN_PROD');
    this.baseUrl = `https://${subdomain}.zendesk.com`;
    this.email = configService.get('EMAIL_ACCESS_PROD');
    this.token = configService.get('TOKEN_ACESS_PROD');
    this.apiZendesk = this.axiosLoad();
  }

  private axiosLoad() {
    return axios.create({
      baseURL: this.baseUrl,
      headers: {
        Authorization: `Basic ${encode(`${this.email}/token:${this.token}`)}`,
      },
    });
  }

  async getAllTicketsByFilter(
    brand: string,
    updated_at: string | Date,
  ): Promise<ITicket[]> {
    const url = `api/v2/search.json?query=
    custom_field_1900001016865%3Atrue+
    type%3Aticket+
    status%3Aclosed+
    updated_at%3E%3D${updated_at}+
    brand%3A${brand}`;
    console.log('url::', url);
    return (
      await this.apiZendesk.get(
        `api/v2/search.json?query=
        custom_field_1900001016865%3Atrue+
        type%3Aticket+
        status%3Aclosed+
        updated_at%3E%3D${updated_at}+
        brand%3A${brand}`,
      )
    )?.data?.results;
  }
}
