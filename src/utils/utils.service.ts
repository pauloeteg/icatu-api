import { HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { IJson } from 'src/modules/layouts/models/ILayouts';
import * as fs from 'fs';
import { ITicket } from 'src/api/dto/response-tickets.dto';

export class UtilsService {
  static generateHash(vlString: string): string {
    if (!vlString)
      throw new HttpException(
        'Não é possivel encriptar um valor vazio! ',
        HttpStatus.BAD_REQUEST,
      );

    return bcrypt.hashSync(vlString, 10);
  }

  static validateHash(vlString: string, vlHash: string): Promise<boolean> {
    return bcrypt.compare(vlString, vlHash || '');
  }

  static addZeroStart(num: string) {
    return num.length === 1 ? '0' + num : num;
  }

  static zeroFill(num: number, len: number) {
    let numberWithZeroes = String(num);
    let counter = numberWithZeroes.length;

    while (counter < len) {
      numberWithZeroes = '0' + numberWithZeroes;
      counter++;
    }

    return numberWithZeroes;
  }

  static changePositionInArray(arr: IJson[], from: number, to: number) {
    arr.splice(to, 0, arr.splice(from, 1)[0]);
    return arr;
  }

  static createFileOfProposal(nameOfFile: string, contentDock: string) {
    fs.writeFile(`./src/files/${nameOfFile}.txt`, contentDock, () =>
      console.log('Criou arquivo do ticket: ', nameOfFile),
    );
  }

  static getValueCustomField(id: string, ticket: ITicket): string {
    const valueCustom = ticket.custom_fields.filter(
      (cf) => cf.id.toString() === id,
    );

    return valueCustom.length == 0 ? ';' : valueCustom[0].value + ';';
  }
}
