**APi desenvolvida para plataforma de gerenciamento de schedules da Icatu.**

-------------------------------------------------------------------

**Especificações técnicas:**
    Projeto construído com nestjs
    Utilizando postgres como banco, versionando com migrations.

-------------------------------------------------------------------

**Scripts:**

   **_yarn start_:** Inicializa o projeto.

   **_yarn dev_:** Inicializa o projeto em modo dev auto reload.

   **_yarn migration:run_:** Executa todos os arquivos de migration.

   **_yarn migration:gen_:** Cria nova migration de acordo com alterações realizadas.

-------------------------------------------------------------------

**Mais informações:**
Por padrão o projeto subirá na porta 5000
